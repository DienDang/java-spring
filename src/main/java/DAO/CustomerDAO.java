package DAO;

import spring.java.examples.Customer;

import java.util.List;

/**
 * Created by dangdien on 4/28/17.
 */
public interface CustomerDAO {
    public int getNumberOfCustomer();
    public Customer getCustomerName(int customerId);
    public List<Customer> getAllCustomer();
    public void insertCustomer(Customer customer);


}
