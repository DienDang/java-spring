package DAO;

import javafx.scene.shape.Circle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import spring.java.examples.Customer;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

/**
 * Created by dangdien on 4/28/17.
 */
@Component
public class CustomerDAOImpls implements CustomerDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public DataSource getDataSource() {
        return dataSource;
    }
    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public int getNumberOfCustomer(){
        String sql = "SELECT COUNT(*) FROM customer";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public Customer getCustomerName(int customerId) {
        String sql = "SELECT * FROM customer WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[] {customerId}, new CustomerMapper());
    }

    public List<Customer> getAllCustomer(){
        String sql = "SELECT * FROM customer";
        return jdbcTemplate.query(sql, new CustomerMapper());
    }


    private static final class CustomerMapper implements RowMapper<Customer>{
        @Override
        public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Customer(resultSet.getInt("id"), resultSet.getString("name"));
        }
    }


    @Override
    public void insertCustomer(Customer customer) {
        String sql = "INSERT INTO customer (id, name) VALUES (? , ?)";
        jdbcTemplate.update(sql, new Object[] {customer.getId(), customer.getName()});
    }
}
