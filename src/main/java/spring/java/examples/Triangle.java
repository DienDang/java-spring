package spring.java.examples;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import spring.java.examples.DrawEvent;

/**
 * Created by dangdien on 4/27/17.
 */
public class Triangle implements ApplicationEventPublisherAware {
//    spring.java.examples.Point pointA;
//    spring.java.examples.Point pointB;
    Point pointC;
    ApplicationEventPublisher publisher;


//    public spring.java.examples.Point getPointA() {
//        return pointA;
//    }
//
//    public void setPointA(spring.java.examples.Point pointA) {
//        this.pointA = pointA;
//    }
//
//    public spring.java.examples.Point getPointB() {
//        return pointB;
//    }
//
//    public void setPointB(spring.java.examples.Point pointB) {
//        this.pointB = pointB;
//    }

    public Point getPointC() {
        return pointC;
    }
    @Autowired
    @Qualifier("circleRelated")
    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }

    public void draw(){
        System.out.println(this.getPointC().getX() );
        DrawEvent event = new DrawEvent(this);
        publisher.publishEvent(event);
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }
}
