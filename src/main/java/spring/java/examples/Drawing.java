package spring.java.examples;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

/**
 * Created by dangdien on 4/27/17.
 */
public class Drawing {
    public static void main(String[] args) {
        ApplicationContext context  = new ClassPathXmlApplicationContext("Spring.xml");
        Triangle triangle = (Triangle) context.getBean("triangle");
        triangle.draw();
    }
}
