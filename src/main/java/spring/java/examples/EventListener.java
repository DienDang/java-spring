package spring.java.examples;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.context.event.ContextStartedEvent;

/**
 * Created by dangdien on 4/28/17.
 */
@Component
public class EventListener implements ApplicationListener {
    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
//        System.out.println(applicationEvent.toString());
    }
}
