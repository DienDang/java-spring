package spring.java.examples;

import org.springframework.context.ApplicationEvent;

/**
 * Created by dangdien on 4/28/17.
 */
public class DrawEvent extends ApplicationEvent {
    public DrawEvent(Object source) {
        super(source);
    }

    public String toString(){
        return "Draw event";
    }
}
