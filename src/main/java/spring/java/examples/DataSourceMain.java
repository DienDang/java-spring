package spring.java.examples;

import DAO.CustomerDAO;
import DAO.CustomerDAOImpls;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by dangdien on 4/28/17.
 */
public class DataSourceMain {
    public static void main(String[] args) {
        ApplicationContext context  = new ClassPathXmlApplicationContext("Spring.xml");
        CustomerDAO dao = (CustomerDAO) context.getBean("customerDAOImpls");
        System.out.println("number of records : " + dao.getNumberOfCustomer());
        System.out.println("name of customer for id 8 : " + dao.getCustomerName(8).getName());
        System.out.println("list of customers");
        for (Customer cus: dao.getAllCustomer()
             ) {
            System.out.println(cus.getId() + " " + cus.getName());
        }
        Customer customer = new Customer(4, "cus4");
        //dao.insertCustomer(customer);
    }
}
